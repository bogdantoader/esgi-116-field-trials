import csv

def readFakeDataMatrix(location):
    # Import cost data
    A = []
    i = 0
    with open(location, 'rb') as csvfile:
        thereader = csv.reader(csvfile, delimiter=',', quotechar='|',quoting=csv.QUOTE_MINIMAL)
        for row in thereader:
            sublist = []
            for elem in row:
                sublist.append(float(elem))
            A.append(sublist)

    return A

def readFakeDataList(location):
    A = []

    with open(location, 'rb') as csvfile:
        thereader = csv.reader(csvfile, delimiter=',', quotechar='|',quoting=csv.QUOTE_MINIMAL)
        for row in thereader:
            for elem in row:
               A.append(float(elem))

    return A
