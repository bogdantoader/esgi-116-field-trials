#!/usr/bin/python

from gurobipy import *
from readFakeData import *
from numpy.random import normal,seed

# Path to the data
# Real data might not be available in the git repository as it is classified
path = './fakeData1/' # Fake data
#path = './NoHeadersData/' # Real data (Matteo)
#path = '../more/Data_for_model/NoHeaders/' # Real data (Bogdan)

def column(matrix, i):
    return [row[i] for row in matrix]

# Create a new model
e = Env()
e.setParam('TimeLimit', .5*60)
m = Model(env=e)

# Import cost data or generate random cost data vector
# At the moment we read it just to get the size
# this is not optimal
C = readFakeDataList(path + 'C_Vector.csv')
lC = len(C)
# Fix random seed
seed(1)
C = abs(normal(2.4e4,1.0e4,lC)).tolist()
C = [min(a,5.e4) for a in C]

# Trade-off constant
eps = 1.0/max(C)

# Import capacity data
D = readFakeDataMatrix(path + 'D_Matrix.csv')
print "D: %d x %d" % (len(D), len(D[0]))
ndelplat = len(D[0])

# Import delivery platform consumption per activity data
A = readFakeDataMatrix(path + 'A_Matrix_1.5.csv')
print "A: %d x %d" % (len(A), len(A[0]))

# Load group numbers
N = readFakeDataList(path + 'N.csv')

P = readFakeDataMatrix(path + 'P_Matrix.csv')
print "X: %d x %d" % (len(P), len(P[0]))

# Create decision variables
nrows = len(P)
ncols = len(P[0])
X = []
for i in range(nrows):
    row = []
    for j in range(ncols):
        x = m.addVar(vtype=GRB.BINARY, name="x_{0}_{1}".format(i,j))
        row.append(x)
    X.append(row)


# Create slack variables to inforce the "group" constraint
n_groups = len(N)
Z = []
for i in range(n_groups):
    z = m.addVar(vtype=GRB.BINARY, name = "z_{0}".format(i))
    Z.append(z)

# Integrate new variables
m.update()

# Set objective: maximise priority
m.setObjective(quicksum([item for i in range(nrows) for item in X[i]]) - eps*quicksum([item for i in range(nrows) for item in [a*b for a,b in zip(X[i],C)]]), GRB.MAXIMIZE)

# Add constraint: remove known unfeasible solutions 
for i in range(nrows):
    for j in range(ncols):
        if P[i][j] > 0.5:
            m.addConstr(X[i][j], GRB.EQUAL, 0)

# Add constraint: capacity constraint 
for i in range(nrows):
    for k in range(ndelplat):
        m.addConstr(quicksum([a*b for a,b in zip(X[i],column(A,k))]) <= D[i][k])

# Add constraint: activities can be allocated only once
for j in range(ncols):
    m.addConstr(quicksum(column(X,j)) <= 1)

# Add constraint: subactivities in the same activity
# are either done all together or they are not done at all
i = 0
from numpy import cumsum
NN = cumsum(N).tolist()
NN.insert(0,0)
NN = [int(a) for a in NN]
for i in range(n_groups):
    m.addConstr(N[i]*Z[i], GRB.EQUAL, quicksum([item for j in range(nrows) for item in X[j][NN[i]:(NN[i+1])]]))
    
m.optimize()

# Extract output data and store it into a numpy array
from numpy import *
X_out = zeros((nrows,ncols))
for i in range(nrows):
    for j in range(ncols):
        X_out[i,j] = X[i][j].x

# Compute overall cost
selected_cost = array(C)*sum(X_out,axis = 0)
selected_cost = [a for a in selected_cost if a > 1.0e-8]

########## Handle histogram plotting
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

# the histogram of the data
n, bins, patches = plt.hist(selected_cost, bins=50, normed=1, facecolor='green', alpha=0.75)
print n
plt.grid(True)

plt.show()

##############

# Print post-processed output

print "With parameter epsilon: %f" % eps

print "Number of activities with multiple subactivities selected: %d" % sum(array([a.x for a in Z]))

occupied_resources_percentage =  100.*(1.0 - sum(array(D) - X_out.dot(array(A)),axis=0)/sum(array(D),axis=0))
print "Percentage of occupied resources: %s" % occupied_resources_percentage

n_selected_activities = sum(X_out)
print "Number of selected activities: %d out of: %d, percentage: %f" % (n_selected_activities,ncols,100.0*n_selected_activities/float(ncols))

print('Obj: %g' % m.objVal)
